Dirble API
=====================================================

MOTIVE
----------
If you’re a music lover, you already know that turning on the tunes can help calm your nerves, make stress disappear, pump up your energy level during work, bring back old memories, as well as prompt countless other emotions too varied to list. Creates a +ve vibe, makes you think more. And oh yes there are many more alternatives to find great music sources rather than this module but give it a try :).

DESCRIPTION
------------------
Dirble API gets stream urls for radio stations based on categories, by making curl request to the API. Start by creating an account at http://dirble.com. In order to make successful api requests to the API server enter the api key provided by dirble, in module configuration page.


REQUIREMENTS
----------------------
1. Download the jplayer library from https://github.com/happyworm/jPlayer/releases on sites/all/libraries/jplayer.
2. PHP curl extension see more http://php.net/manual/en/curl.setup.php


INSTALLATION
--------------------
1. Download and unpack the "Dirble API" module in your modules folder
   (this will usually be "sites/all/modules/").
2. Make sure PHP CURL extension is installed, in your system.
3. Go to "Administer" -> "Modules" and enable the "Dirble API"
   module

CONFIGURATION
-----------------------
1. Please check on the permissions page for the Dirble API module.
2. Once module is installed and enabled goto "Configuration" -> "Media" tab and click on "Setup Dirble API" link.
3. You will need to register free at http://dirble.com to get your API key. Once done enjoy live audio streaming from thousands of stations.
4. Search for live radio stations from "Configuration" -> "Media" -> "Setup dirble API" -> "Radio Streams"


Known Issues
------------------
1. jPlayer cannot play "aac" audio stream in firefox.

Remaining
-----------------
1. Pagination.
2. Search station by country.