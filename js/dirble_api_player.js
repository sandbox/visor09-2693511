/**
 * @file
 */
jQuery(document).ready(function($){
	var track = '';
	var id = '';
	var title = '';
	var	my_jPlayer = $("#jquery_jplayer");
	my_jPlayer.jPlayer({		
		swfPath: "jplayer",
		cssSelectorAncestor: "#jp_container",
		cssSelector: {          
			play: ".jp-play",
			pause: ".jp-pause",          
			mute: ".jp-mute",
			unmute: ".jp-unmute",
			volumemax: ".jp-volume-max",
			title: ".jp-title"
		},
		supplied: "m4a",
		wmode: "window",
		useStateClassSkin: false,
		autoBlur: false,
		keyEnabled: true
	});

	my_jPlayer.bind($.jPlayer.event.play, function(event){
		$('#'+id).removeClass('def');
		$('#'+id).addClass('playing');
		$('#'+id).html('Stop');
	});

	my_jPlayer.bind($.jPlayer.event.pause, function(event){
		$('#'+id).removeClass('playing');
		$('#'+id).addClass('def');
		$('#'+id).html('Play');
	});

	my_jPlayer.bind($.jPlayer.event.error, function(event){
		$('#'+id).removeClass('def');
		$('#'+id).addClass('playing');
		$('#'+id).html('Stop');		
	});	

	Drupal.ajax.prototype.commands.dirble_api_player_bind_play = function(ajax, response, status){				
		/*Create click handlers for the different tracks*/
		$("#dirble-api-data  a.channel-play").click(function(e) {
			if(($(this).attr('id') !== track) && track != ''){
				$('#'+track).addClass('def');
				$('#'+track).removeClass('playing');
				$('#'+track).html('Play');
			}
			track = $(this).attr('id');
			e.preventDefault();											
			if ($(this).hasClass('def')) {
				id = $(this).attr('id');
				title = $('#title-' + id + ' a').html();
				is_playing = this;
				//the audio streaming needs to be started
				$(this).removeClass('def');
				$(this).addClass('playing');
				my_jPlayer.jPlayer("setMedia", {
					m4a: $(this).attr('href'),
					title: title
				});

				my_jPlayer.jPlayer("play");
				$(this).html('Stop');
			} 
			else if($(this).hasClass('playing')) {
				//the audio streaming needs to be paused
				$(this).addClass('def');
				$(this).removeClass('playing');	
				my_jPlayer.jPlayer("pause");
				$(this).html('Play');				
			}						
			return false;
		});	
	};
});