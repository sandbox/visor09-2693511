<?php
/**
 * @file
 */

/**
 * API configuration form
 */
function dirble_api_setup_form($form, $form_state) {

  $checkcurl = _isCurl();
  if (empty($checkcurl)) {
    drupal_set_message('PHP curl extension not installed.', 'error');
    return $form;
  }

  $form['dirble_api_setup_fiedset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Dirble API Setup'),
    '#description' => t('Dirble API settings, setup dirble API key, API URL and version'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    );

  $form['dirble_api_setup_fiedset']['status_messages'] = array(
    '#markup' => '<div id="dirble-api-form-status-messages"></div>',
    );

  $form['dirble_api_setup_fiedset']['dirble_api_apikey'] = array(
    '#type' => 'textfield',
    '#title' => t('Dirble api key'),
    '#description' => t('Register at <a href="https://dirble.com">Drible.com</a>, 
      choose your plan and get your api key in user accout console'),
    '#default_value' => variable_get('dirble_apikey', ''),
    '#required' => TRUE,
    '#maxlength' => 50,
    );

  $form['dirble_api_setup_fiedset']['dirble_api_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Dirble api url'),
    '#description' => t('Base url for Drible API'),
    '#default_value' => variable_get('dirble_apiurl', 'http://api.dirble.com'),
    '#required' => TRUE,
    );

  $form['dirble_api_setup_fiedset']['dirble_api_version'] = array(
    '#type' => 'select',
    '#title' => t('API version'),
    '#description' => t('This module uses version 2 to make request to the Dirble server'),
    '#options' => array('v2' => 'v2'),
    '#default_value' => variable_get('dirble_api_version', 'v2'),
    );

  $form['dirble_api_setup_fiedset']['dirble_api_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#ajax' => array(
      'callback' => 'dirble_api_setup_form_ajax_submit',
      'wrapper' => 'dirble-api-form-status-messages',
      'effect' => 'fade',
      'method' => 'replace'
      ),    
    );

  return $form;
}

/**
 * API configuration form ajax callback function
 */
function dirble_api_setup_form_ajax_submit($form, $form_state) {  
  if (!form_get_errors()) {
    $dirble_apikey = check_plain(trim($form_state['values']['dirble_api_apikey']));
    $dirble_apiurl = check_plain(trim($form_state['values']['dirble_api_url']));
    $dirble_api_version = $form_state['values']['dirble_api_version'];
    variable_set('dirble_apikey', $dirble_apikey);
    variable_set('dirble_apiurl', $dirble_apiurl);
    variable_set('dirble_api_version', $dirble_api_version);
    _dirble_api_cache_categories($dirble_apiurl, $dirble_api_version, $dirble_apikey, 1);
    drupal_set_message(t('Dirble api settings succesfully saved'));
  }
  return '<div id="dirble-api-form-status-messages">' . theme("status_messages") . '</div>';
}


/**
 * Get streams form, player interface
 */
function dirble_api_stream_configure_form($form, $form_state) {
  if (empty(variable_get('dirble_apikey', ''))) {
    $form['error'] = array(
      '#markup' => '
      <div class="help-text messages error">
        Dirble API key not set. Start by creating an account at <a target="_blank" href="https://dirble.com/users/sign_up">Dirble</a>, on succesfull completion get your free api key by following the link <a target="_blank" href="https://dirble.com/users/apikeys">API keys</a> and add it to the module configuration <a href="/admin/config/media/dirble_api/setup">page</a>.      
      </div>'
      );    
    return $form;
  }


  $check_library = libraries_detect('jplayer');
  if (empty($check_library['installed'])) {
    $form['error'] = array(
      '#markup' => '
      <div class="help-text messages error">      
        jPlayer library not found.Download the jplayer library from <a href="https://github.com/happyworm/jPlayer/releases">jPlayer</a> on sites/all/libraries/jplayer.
      </div>'
      );  
    return $form;
  }

  $apiurl = variable_get('dirble_apiurl');
  $apiversion = variable_get('dirble_api_version');
  $dirbleapikey = variable_get('dirble_apikey');  

  $data = _dirble_api_cache_categories($apiurl, $apiversion, $dirbleapikey, 0);  

  $category = array();
  foreach ($data as $key => $value) {
    $category[$value->id] = $value->title;
  }  

  $form['dirble_api_wrapper'] = array(
    '#type' => 'fieldset',
    '#collapsed' => FALSE,
    '#collapsible' => TRUE,
    '#title' => t('Search Stations'),
    );

  $form['dirble_api_wrapper']['dirble_api_categories'] = array(
    '#type' => 'select',
    '#title' => t('Categories / Genre'),
    '#description' => t('Get Radio Stations with respect to categories'),
    '#options' => $category,
    '#prefix' => '<div class="dirble-col col-one">'
    );

  $player = theme('audio_streaming_player');

  $form['dirble_api_wrapper']['submit'] = array(
    '#type' => 'button',
    '#value' => t('Search'),
    '#ajax' => array(
      'callback' => '_dirble_api_get_live_streams_genre',
      'wrapper' => 'dirble-api-data',
      'effect' => 'fade',
      'method' => 'replace'
      ),
    '#suffix' => '</div>',
    );

  $form['dirble_api_wrapper']['dirble_api_player'] = array(
    '#prefix' => '<div class="dirble-col col-two">',
    '#markup' => $player,
    '#suffix' => '</div>',
    );     

  $form['dirble_api_wrapper']['dirble_api_search_data'] = array(
    '#markup' => '<div id="dirble-api-data"></div>',    
    );

  $module_path = drupal_get_path('module', 'dirble_api');

  $form['#attached']['libraries_load'][] = array('jplayer');
  
  $form['#attached']['js'] = array(
    $module_path . '/js/dirble_api_player.js',
    );

  $form['#attached']['css'] = array(
    $module_path . '/css/dirble_api.css',
    );
  return $form;
}

/**
  * Get streams ajax callback function
  */
function _dirble_api_get_live_streams_genre($form, &$form_state) {  
  $category_id = $form_state['values']['dirble_api_categories'];
  $apiurl = variable_get('dirble_apiurl');
  $apiversion = variable_get('dirble_api_version');
  $dirbleapikey = variable_get('dirble_apikey');
  $curlurl = $apiurl . '/' . $apiversion . '/category/' . $category_id . '/stations?token=' . $dirbleapikey;
  //make the curl request  
  $api_data = _dirle_api_get_result($curlurl);
  $no_image_path = drupal_get_path('module', 'dirble_api') . '/noimage.png';
  
  $channelcnt =  0;
  $output = '';  
  //check if returned response has any data
  if (!empty($api_data)) {
    foreach ($api_data as $key => $value) {
      //check for station id, needed to check the received result.
      if (isset($value->id) && !empty($value->id)) {
        $channelcnt++;
        $temp = $value->image;
        $img = empty($temp->url) ? '/' . $no_image_path : $temp->url;
        $stream = $value->streams;
        //status == 1 means the stream is up and running
        if ($stream[0]->status == 1) {
          $link = $stream[0]->stream;
          $start = l(t('Play'), $link, array('attributes' => array('class' => array('channel-play', 'def'), 'id' => 'track-' . $value->id)));        
          $controls = $start;
          $status = 'active';
        }
        else {
          $link = '<b class="link-down">LINK IS DOWN</b>';
          $controls = $link;
          $status = 'inactive';
        }
        $name = strlen($value->name) > 40 ? substr($value->name, 0, 40) : $value->name;
        $title = l($name, $value->website, array('attributes' => array('target' => '_blank')));
        $output .= '
        <div class="channel channel-' . $value->id . ' ' . $status . '">
          <div class="channel-img-holder">
            <img class="channel-img" src="' . $img . '" alt="' . $value->name . '" />
          </div>
          <div class="channel-description">
            <div class="channel-title" id="title-track-' . $value->id . '">' . $title . '</div>          
            <div class="channel-controls">' . $controls . '</div>
            <div class="channel-country"><span>country : </span>' . $value->country . '</div>
          </div>
        </div>';
      }
    }//end foreach
    if ($channelcnt == 0) {
      $output .='<div class="channel-error"><h2>No channels found, search again</h2></div>';
    }   
  }

  $commands = array();
  $commands[] = ajax_command_replace('#dirble-api-data', '<div id="dirble-api-data">' . $output . '</div>');
  $commands[] = array('command' => 'dirble_api_player_bind_play');
  return array('#type' => 'ajax', '#commands' => $commands);
}


/**
 * Custom function which gets all the categories, the moment user fills all the API Details.
 * This functionality is implemented in order to avoid multiple calls to the API server
 * to get categories.
 */
function _dirble_api_cache_categories($dirble_apiurl, $dirble_api_version, $dirble_apikey, $action = 1) {
  $categorycached = db_select('dirble_api_miscellaneous', 'dam')
  ->fields('dam')
  ->condition('api_type', 'category', '=')
  ->execute()
  ->rowCount();

  if ($categorycached == 0 && $action == 1) {
    /*Get all Music Genre*/
    $curlurl = $dirble_apiurl . '/' . $dirble_api_version . '/categories?token=' . $dirble_apikey;
    $categoryresult = _dirle_api_get_result($curlurl);
    $result = db_insert('dirble_api_miscellaneous')
    ->fields(array(
      'api_data' => serialize($categoryresult),
      'api_type' => 'category'
      ))
    ->execute();    
  } 
  elseif ($action == 0) {
    $categoryresult = db_select('dirble_api_miscellaneous', 'dam')
    ->fields('dam')
    ->condition('api_type', 'category', '=')
    ->execute()
    ->fetchAll(); 
    $categoryresult = unserialize($categoryresult[0]->api_data);    
  } 
  return $categoryresult;
}

/**
 * custom function checks if php-curl pacakge is installed and enabled 
 * or not.
 */
function _isCurl(){
  return function_exists('curl_version');
}