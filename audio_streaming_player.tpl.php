<div id="jquery_jplayer" class="jp-player"></div>
<div id="jp_container">
	<h3 class="jp-title"></h3>
	<div class="jp-controls">
		<button class="jp-play" title="Play" role="button" tabindex="0"></button>
		<button class="jp-pause" title="Pause" role="button" tabindex="0"></button>
	</div>
	<div class="jp-mute-control">
		<button class="jp-mute" title="Mute" role="button" tabindex="0"></button>
		<button class="jp-unmute" title="Unmute" role="button" tabindex="0"></button>
	</div>
	<div class="jp-volume-bar">		
		<div class="jp-volume-bar-value"></div>
	</div>
	<div class="jp-volume-control">
		<button class="jp-volume-max" title="Volume up" role="button" tabindex="0"></button>
	</div>
</div>